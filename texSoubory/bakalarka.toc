\select@language {british}
\select@language {czech}
\select@language {british}
\select@language {czech}
\select@language {czech}
\select@language {czech}
\contentsline {chapter}{\numberline {1}\IeC {\'U}vod}{9}{chapter.1}
\contentsline {chapter}{\numberline {2}Indexace}{10}{chapter.2}
\contentsline {section}{\numberline {2.1}Postup indexace}{10}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Tokenizace}{10}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Odstran\IeC {\v e}n\IeC {\'\i } nev\IeC {\'y}znamov\IeC {\'y}ch slov}{11}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Normalizace}{11}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Invertovan\IeC {\'y} seznam}{12}{subsection.2.1.4}
\contentsline {chapter}{\numberline {3}Reprezentace textov\IeC {\'y}ch dokument\IeC {\r u}}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Bag of words}{13}{section.3.1}
\contentsline {section}{\numberline {3.2}Booleovsk\IeC {\'y} model}{13}{section.3.2}
\contentsline {section}{\numberline {3.3}Vektorov\IeC {\'y} model}{14}{section.3.3}
\contentsline {section}{\numberline {3.4}Latent Semantic Analysis}{15}{section.3.4}
\contentsline {section}{\numberline {3.5}Latent Dirichlet Allocation}{16}{section.3.5}
\contentsline {chapter}{\numberline {4}V\IeC {\'a}hov\IeC {\'a}n\IeC {\'\i } term\IeC {\r u}}{17}{chapter.4}
\contentsline {section}{\numberline {4.1}Bin\IeC {\'a}rn\IeC {\'\i } v\IeC {\'a}ha}{17}{section.4.1}
\contentsline {section}{\numberline {4.2}Frekvence termu}{17}{section.4.2}
\contentsline {section}{\numberline {4.3}Inverzn\IeC {\'\i } frekvence dokumentu}{18}{section.4.3}
\contentsline {section}{\numberline {4.4}tf-idf}{18}{section.4.4}
\contentsline {chapter}{\numberline {5}Porovn\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } dokument\IeC {\r u}}{20}{chapter.5}
\contentsline {section}{\numberline {5.1}Euklidovsk\IeC {\'a} vzd\IeC {\'a}lenost}{20}{section.5.1}
\contentsline {section}{\numberline {5.2}Kosinov\IeC {\'a} podobnost}{20}{section.5.2}
\contentsline {section}{\numberline {5.3}Jaccard\IeC {\r u}v index}{21}{section.5.3}
\contentsline {section}{\numberline {5.4}Kullback-Leibler divergence}{22}{section.5.4}
\contentsline {chapter}{\numberline {6}Vyhodnocen\IeC {\'\i } v\IeC {\'y}sledk\IeC {\r u}}{23}{chapter.6}
\contentsline {section}{\numberline {6.1}Precision}{23}{section.6.1}
\contentsline {section}{\numberline {6.2}\IeC {\'U}plnost}{23}{section.6.2}
\contentsline {section}{\numberline {6.3}Accuracy}{23}{section.6.3}
\contentsline {section}{\numberline {6.4}F-m\IeC {\'\i }ra}{24}{section.6.4}
\contentsline {chapter}{\numberline {7}Detekce t\IeC {\'e}m\IeC {\v e}\IeC {\v r} duplicitn\IeC {\'\i }ch dokument\IeC {\r u}}{25}{chapter.7}
\contentsline {section}{\numberline {7.1}Shingling}{25}{section.7.1}
\contentsline {chapter}{\numberline {8}Rozpozn\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } pojmenovan\IeC {\'y}ch entit}{27}{chapter.8}
\contentsline {section}{\numberline {8.1}N\IeC {\'a}stroje pro anglick\IeC {\'y} jazyk}{27}{section.8.1}
\contentsline {section}{\numberline {8.2}N\IeC {\'a}stroj pro \IeC {\v c}esk\IeC {\'y} jazyk}{28}{section.8.2}
\contentsline {chapter}{\numberline {9}Apache Lucene}{31}{chapter.9}
\contentsline {section}{\numberline {9.1}Podobnost v Lucene}{31}{section.9.1}
\contentsline {section}{\numberline {9.2}Lucene scoring formula}{32}{section.9.2}
\contentsline {section}{\numberline {9.3}More like this}{33}{section.9.3}
\contentsline {section}{\numberline {9.4}Apache Tika}{33}{section.9.4}
\contentsline {chapter}{\numberline {10}Revize}{34}{chapter.10}
\contentsline {section}{\numberline {10.1}Definice}{34}{section.10.1}
\contentsline {section}{\numberline {10.2}P\IeC {\v r}\IeC {\'\i }klady reviz\IeC {\'\i }}{35}{section.10.2}
\contentsline {chapter}{\numberline {11}Testovac\IeC {\'\i } mno\IeC {\v z}ina dokument\IeC {\r u}}{37}{chapter.11}
\contentsline {chapter}{\numberline {12}Implementace st\IeC {\'a}vaj\IeC {\'\i }c\IeC {\'\i }ch algoritm\IeC {\r u}}{38}{chapter.12}
\contentsline {section}{\numberline {12.1}Anal\IeC {\'y}za v\IeC {\'y}b\IeC {\v e}ru algoritm\IeC {\r u}}{38}{section.12.1}
\contentsline {section}{\numberline {12.2}Pou\IeC {\v z}it\IeC {\'e} technologie}{38}{section.12.2}
\contentsline {section}{\numberline {12.3}Vytvo\IeC {\v r}en\IeC {\'\i } indexu}{38}{section.12.3}
\contentsline {section}{\numberline {12.4}TF-IDF a kosinov\IeC {\'a} vzd\IeC {\'a}lenost}{40}{section.12.4}
\contentsline {section}{\numberline {12.5}K-shingling}{41}{section.12.5}
\contentsline {section}{\numberline {12.6}More like this}{42}{section.12.6}
\contentsline {section}{\numberline {12.7}Pr\IeC {\r u}b\IeC {\v e}h experiment\IeC {\r u}}{42}{section.12.7}
\contentsline {section}{\numberline {12.8}V\IeC {\'y}sledek experiment\IeC {\r u}}{42}{section.12.8}
\contentsline {chapter}{\numberline {13}Implementace vlastn\IeC {\'\i }ho algoritmu}{44}{chapter.13}
\contentsline {section}{\numberline {13.1}Anal\IeC {\'y}za a n\IeC {\'a}vrh \IeC {\v r}e\IeC {\v s}en\IeC {\'\i }}{44}{section.13.1}
\contentsline {section}{\numberline {13.2}Popis \IeC {\v r}e\IeC {\v s}en\IeC {\'\i }}{44}{section.13.2}
\contentsline {section}{\numberline {13.3}V\IeC {\'y}sledky testov\IeC {\'a}n\IeC {\'\i }}{46}{section.13.3}
\contentsline {chapter}{\numberline {14}Z\IeC {\'a}v\IeC {\v e}r}{48}{chapter.14}
\contentsline {chapter}{Literatura}{49}{chapter*.6}
\contentsline {chapter}{Seznam p\IeC {\v r}\IeC {\'\i }loh}{52}{appendix*.7}
\contentsline {chapter}{\numberline {A}V\IeC {\'y}sledky testov\IeC {\'a}n\IeC {\'\i } nov\IeC {\'e}ho algoritmu}{53}{appendix.A}
\contentsline {section}{\numberline {A.1}Jednotliv\IeC {\'e} termy ohodnocen\IeC {\'e} frekvenc\IeC {\'\i } termu}{53}{section.A.1}
\contentsline {section}{\numberline {A.2}Jednotliv\IeC {\'e} termy ohodnocen\IeC {\'e} TF-IDF}{53}{section.A.2}
\contentsline {section}{\numberline {A.3}Shingly velikosti 2 ohodnocen\IeC {\'e} frekvenc\IeC {\'\i } termu}{54}{section.A.3}
\contentsline {section}{\numberline {A.4}Shingly velikosti 3 ohodnocen\IeC {\'e} frekevenc\IeC {\'\i } termu}{54}{section.A.4}
\contentsline {section}{\numberline {A.5}Shingly velikosti 4 ohodnocen\IeC {\'e} frekvenc\IeC {\'\i } termu}{55}{section.A.5}
\contentsline {section}{\numberline {A.6}Shingly velikosti 5 ohodnocen\IeC {\'e} frekvenc\IeC {\'\i } termu}{55}{section.A.6}
\contentsline {chapter}{\numberline {B}U\IeC {\v z}ivatelsk\IeC {\'a} p\IeC {\v r}\IeC {\'\i }ru\IeC {\v c}ka}{56}{appendix.B}
\contentsline {section}{\numberline {B.1}Spu\IeC {\v s}t\IeC {\v e}n\IeC {\'\i } programu}{56}{section.B.1}
\contentsline {section}{\numberline {B.2}Indexace dokument\IeC {\r u}}{56}{section.B.2}
\contentsline {section}{\numberline {B.3}Experiment s jednotliv\IeC {\'y}mi termy s TF-IDF a kosinovou vzd\IeC {\'a}lenost\IeC {\'\i }}{57}{section.B.3}
\contentsline {section}{\numberline {B.4}Experiment s k-shinglingem}{57}{section.B.4}
\contentsline {section}{\numberline {B.5}Experiment s More Like this}{58}{section.B.5}
\contentsline {section}{\numberline {B.6}Experiment s nov\IeC {\'y}m algoritmem}{58}{section.B.6}
