package index;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

public class VytvoreniIndexu {
	
	public static final FieldType TYPE_STORED = new FieldType();
	
	// nastaveni ukladani celeho textu do indexu a udrzovani vektoru dokumentu
    static {
    	TYPE_STORED.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
        TYPE_STORED.setTokenized(true);
        TYPE_STORED.setStoreTermVectors(true);
        TYPE_STORED.setStoreTermVectorPositions(true);
        TYPE_STORED.setStored(true);
        TYPE_STORED.freeze();
    }
	
    
    /**
     * Pomoci nastroje Tika se extrahuje text a metadata z dokumentu a ty jsou pote zaindexovany
     * @param indexDirectory slozka pro ulozeni indexu
     * @param sourceDirectory slozka s dokumenty pro zaindexovani
     * @param analyzer pouzity analyzer
     * @throws IOException
     */
	public void createIndex(String indexDirectory, String sourceDirectory, Analyzer analyzer) throws IOException{
		File docs = new File(sourceDirectory);
		File[] files = docs.listFiles();	
		Directory directory = FSDirectory.open(Paths.get(indexDirectory));
		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		IndexWriter writer = new IndexWriter(directory, config);		
		writer.deleteAll();		
		for (File file : files) {
			Metadata metadata = new Metadata();
			BodyContentHandler handler = new BodyContentHandler(-1);
			ParseContext context = new ParseContext();
			Parser parser = new AutoDetectParser();
			InputStream stream = null;
			try {
				stream = new FileInputStream(file);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				System.out.println("Nenalezeny dokumenty k zaindexovani!");
				e1.printStackTrace();
				System.exit(1);
			}			
			try {
				parser.parse(stream, handler, metadata, context);
			} catch (SAXException | TikaException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				stream.close();
			}
			String fileName = file.getName();		
			Document doc = new Document();
			StringField filename = new StringField("fileName", fileName, Store.YES);			
			Field content = new Field("text", handler.toString(), TYPE_STORED);
			doc.add(filename);
			doc.add(content);			
			for (String key : metadata.names()) {
				String name = key.toLowerCase();
				String value = metadata.get(key);
				if(value.isEmpty() == true){
					continue;
				}
				doc.add(new StringField(name, value, Store.YES));
			}
			writer.addDocument(doc);
		}		
		writer.commit();
		writer.deleteUnusedFiles();
		writer.close();
	}

}
