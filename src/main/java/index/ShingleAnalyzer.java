package index;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.shingle.ShingleFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;

public class ShingleAnalyzer extends Analyzer{
	
	int minShingle, maxShingle;
	
	public ShingleAnalyzer(int minShingle, int maxShingle) {
		this.minShingle = minShingle;
		this.maxShingle = maxShingle;
	}
	
	@Override
	protected TokenStreamComponents createComponents(String arg0) {
	    StandardTokenizer source = new StandardTokenizer();
	    TokenStream tokenStream = new StandardFilter(source);
	    ShingleFilter sf = new ShingleFilter(tokenStream, minShingle, maxShingle);
	    sf.setOutputUnigrams(false);
		return new TokenStreamComponents(source, sf);
	}	
}
