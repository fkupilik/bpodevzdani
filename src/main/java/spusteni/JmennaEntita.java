package spusteni;

public class JmennaEntita {
	
	private String hodnota, typ;
	private int pozice;
	
	public JmennaEntita(String hodnota, String typ, int pozice){
		this.hodnota = hodnota;
		this.typ = typ;
		this.pozice = pozice;
	}	

	public int getPozice() {
		return pozice;
	}

	public void setPozice(int pozice) {
		this.pozice = pozice;
	}

	public String getHodnota() {
		return hodnota;
	}

	public void setHodnota(String hodnota) {
		this.hodnota = hodnota;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}
	
	public String toString(){
		String navrat = hodnota + " " + typ + " " + pozice;
		return navrat;
	}

}
