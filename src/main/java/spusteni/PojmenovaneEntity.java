package spusteni;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;

import cz.cuni.mff.ufal.nametag.Forms;
import cz.cuni.mff.ufal.nametag.NamedEntities;
import cz.cuni.mff.ufal.nametag.NamedEntity;
import cz.cuni.mff.ufal.nametag.Ner;
import cz.cuni.mff.ufal.nametag.TokenRange;
import cz.cuni.mff.ufal.nametag.TokenRanges;
import cz.cuni.mff.ufal.nametag.Tokenizer;

public class PojmenovaneEntity {
	
	Ner ner;
	
	public PojmenovaneEntity(Ner ner){
		this.ner = ner;
	}
	
	/**
	 * Najde entity v obou dokumentech a porovna jejich hodnoty
	 * @param reader index reader
	 * @param doc1 dokument 1
	 * @param doc2 dokument 2
	 * @return true kdyz se jedna o revize, false v opacnem pripade
	 * @throws IOException
	 */
	public boolean zkontrolujRevize(IndexReader reader, int doc1, int doc2) throws IOException{
		Document document1 = reader.document(doc1);
		Document document2 = reader.document(doc2);
		
		List<JmennaEntita> entity1 = najdiEntity(document1.get("text"));
		List<JmennaEntita> entity2 = najdiEntity(document2.get("text"));
		
		if(porovnejEntityVPrvniTretine(entity1, entity2) == true || porovnejEntityVPosledniCtvrtine(entity1, entity2) == true){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Porovna hodnoty nalezenych entit v posledni ctvrtine dokumentu
	 * @param doc1
	 * @param doc2
	 * @return
	 */
	public boolean porovnejEntityVPosledniCtvrtine(List<JmennaEntita> doc1, List<JmennaEntita> doc2) {
		int tretina = doc1.size() / 4;
		int pocetOdpovidajicich = 0;
		int pocetSpatnych = 0;
		for (int i = 3*tretina; i < doc1.size(); i++) {
			JmennaEntita prvni = doc1.get(i);
			if (prvni.getTyp() != null) {
//				System.out.println(prvni.getHodnota() + " " + prvni.getTyp());
				if(i >= doc2.size()){
					continue;
				}
				JmennaEntita druha = doc2.get(i);
				if (prvni.getHodnota().equals(druha.getHodnota()) || podivejSeOkolo(prvni.getHodnota(), doc2, i) == true) {
//				if(prvni.getHodnota().equals(druha.getHodnota())){
					pocetOdpovidajicich++;
				} else {
					pocetSpatnych++;
				}
			}
		}
		double vysledek = (double)pocetSpatnych/(pocetOdpovidajicich+pocetSpatnych);
		double finish = 1-vysledek;
		if(finish > 0.9){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Porovna hodnoty nalezenych entit v prvni tretine dokumentu
	 * @param doc1
	 * @param doc2
	 * @return
	 */
	public boolean porovnejEntityVPrvniTretine(List<JmennaEntita> doc1, List<JmennaEntita> doc2) {
		int tretina = doc1.size() / 3;
		int pocetOdpovidajicich = 0;
		int pocetSpatnych = 0;
		for (int i = 0; i < tretina; i++) {
			JmennaEntita prvni = doc1.get(i);
			if (prvni.getTyp() != null) {
//				System.out.println(prvni.getHodnota() + " " + prvni.getTyp());
				if(i >= doc2.size()){
					continue;
				}
				JmennaEntita druha = doc2.get(i);
				if (prvni.getHodnota().equals(druha.getHodnota()) || podivejSeOkolo(prvni.getHodnota(), doc2, i) == true) {
//				if(prvni.getHodnota().equals(druha.getHodnota())){
					pocetOdpovidajicich++;
				} else {
					pocetSpatnych++;
				}
			}
		}
		double vysledek = (double)pocetSpatnych/(pocetOdpovidajicich+pocetSpatnych);
		double finish = 1-vysledek;
		if(finish > 0.9){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * zkontroluje, zda se entita stejne hodnoty nenachazi v okoli 100 znaku od pozice entity v prvnim dokumentu/
	 * @param hodnota
	 * @param doc2
	 * @param pozice
	 * @return
	 */
	public boolean podivejSeOkolo(String hodnota, List<JmennaEntita> doc2, int pozice) {
		int dolniPrah = pozice - 100;
		int horniPrah = pozice + 100;
		for (int i = dolniPrah; i <= horniPrah; i++) {
			if(i < 0 || i >= doc2.size()){
				continue;
			}
			if (doc2.get(i).getHodnota().equals(hodnota)) {
//				System.out.println("Hele, stejne jmeno je na pozici " + i);
				return true;
			}
		}
		return false;
	}
	
	public List<JmennaEntita> najdiEntity(String text) {

		List<JmennaEntita> slova = new ArrayList<>();

		Forms forms = new Forms();
		TokenRanges tokens = new TokenRanges();
		NamedEntities entities = new NamedEntities();
		ArrayList<NamedEntity> sortedEntities = new ArrayList<NamedEntity>();

		Tokenizer tokenizer = ner.newTokenizer();
		tokenizer.setText(text);
		int pozice = 0;
		while (tokenizer.nextSentence(forms, tokens)) {
			ner.recognize(forms, entities);
			sortEntities(entities, sortedEntities);
			for (int i = 0, e = 0; i < tokens.size(); i++) {
				;
				TokenRange token = tokens.get(i);
				int token_start = (int) token.getStart();
				int token_end = (int) token.getStart() + (int) token.getLength();

				String entita = null;
				for (; e < sortedEntities.size() && sortedEntities.get(e).getStart() == i; e++) {
					entita = sortedEntities.get(e).getType();
				}
				slova.add(new JmennaEntita(encodeEntities(text.substring(token_start, token_end)), entita, pozice));
				pozice++;
			}
		}
		return slova;
	}
	
	public static String encodeEntities(String text) {
		return text.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\"", "&quot;");
	}

	public static void sortEntities(NamedEntities entities, ArrayList<NamedEntity> sortedEntities) {
		class NamedEntitiesComparator implements Comparator<NamedEntity> {
			public int compare(NamedEntity a, NamedEntity b) {
				if (a.getStart() < b.getStart())
					return -1;
				if (a.getStart() > b.getStart())
					return 1;
				if (a.getLength() > b.getLength())
					return -1;
				if (a.getLength() < b.getLength())
					return 1;
				return 0;
			}
		}
		NamedEntitiesComparator comparator = new NamedEntitiesComparator();

		sortedEntities.clear();
		for (int i = 0; i < entities.size(); i++)
			sortedEntities.add(entities.get(i));
		Collections.sort(sortedEntities, comparator);
	}

}
