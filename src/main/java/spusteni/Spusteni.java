package spusteni;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queries.mlt.MoreLikeThis;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import cz.cuni.mff.ufal.nametag.Ner;
import metriky.IMereni;

public class Spusteni {

	/**
	 * Pro vsechny testovaci dokumenty vyhleda revize a zmeri uspesnost, pricemz pouzije metriku definovanou v parametru
	 * @param indexDirectory
	 * @param testovaciDokumenty
	 * @param minPrah
	 * @param maxPrah
	 * @param krok
	 * @param metrika
	 * @throws IOException
	 */
	public void prohledejDokumenty(String indexDirectory, String testovaciDokumenty, double minPrah, double maxPrah,
			double krok, IMereni metrika) throws IOException {
		File trenovaci = new File(testovaciDokumenty);
		File[] files = trenovaci.listFiles();

		Directory directory = FSDirectory.open(Paths.get(indexDirectory));

		IndexReader reader = DirectoryReader.open(directory);
		IndexSearcher searcher = new IndexSearcher(reader);

		int pocetDokumentu = reader.numDocs();

		Map<String, Double>[] vektory = metrika.vratVahy(indexDirectory); // ohodnoti vektory dokumentu v indexu
		for (double prah = minPrah; prah <= maxPrah; prah += krok) {
			int m = 0;
			double[] fMiry = new double[files.length];

			for (File file : files) {
				int pocetObdrzenych = 0;
				int pocetObdrzenychRelevantnich = 0;

				int cisloDokumentu = -1;

				String jmeno = file.getName();

				String[] rozdelene = jmeno.split("_");

				try {
					cisloDokumentu = Integer.parseInt(rozdelene[0]);
				} catch (Exception e) {
					
				}

				TermQuery query = new TermQuery(new Term("fileName", jmeno));

				TopDocs hits = searcher.search(query, 1); // najde ID prave zkoumaneho dokumentu v indexu

				int docID = 0;

				for (ScoreDoc scoreDoc : hits.scoreDocs) {
					docID = scoreDoc.doc;
				}

				Document d = reader.document(docID);
				int pocetRelevantnich;
				if (d.get("custom:pocetrevizi") == null) {
					pocetRelevantnich = 0;
					pocetObdrzenychRelevantnich = 0;
				} else {
					pocetRelevantnich = (int) Double.parseDouble(d.get("custom:pocetrevizi"));
				}
				// System.out.println("\n Dokument " + d.get("fileName") + " ma
				// mit pocet revizi "
				// + d.get("custom:pocetrevizi") + " ma nasledujici revize\n");
				for (int k = 0; k < pocetDokumentu; k++) {
					if (docID == k) {
						continue;
					}

					double podobnost = metrika.vratMiru(vektory[k], vektory[docID]);
					if (podobnost > prah) {
						pocetObdrzenych++;
						Document a = reader.document(k);
						String[] split = a.get("fileName").split("_");
						try {
							int cislo = Integer.parseInt(split[0]);
							if (cisloDokumentu == cislo) {
								pocetObdrzenychRelevantnich++;
							}
						} catch (Exception e) {

						}
						// System.out.println(k + " " + a.get("fileName") + "
						// pocet revizi " + a.get("custom:pocetrevizi") + " " +
						// podobnost);
					}
				}

				if (pocetRelevantnich == 0 && pocetObdrzenych != 0) {
					fMiry[m] = 0;
				} else if (pocetObdrzenychRelevantnich == 0 && pocetRelevantnich != 0) {
					fMiry[m] = 0;
				} else {
					double mira = spoctiFMiru(pocetObdrzenychRelevantnich, pocetObdrzenych, pocetRelevantnich);
					fMiry[m] = mira;
				}
				m++;
			}
			System.out.println("Pro prah " + prah + " je prumerna F-mira " + ziskejPrumer(fMiry));
		}
	}

	/**
	 * Pro vsechny testovaci dokumenty vyhleda revize a zmeri uspesnost, pricemz pouzije metriku definovanou v parametru.
	 * KL divergence musi mit svoji metodu, jelikoz neni symetricka a tudiz je treba zjistit mezi dokumenty 2 hodnoty. 
	 * @param indexDirectory
	 * @param testovaciDokumenty
	 * @param slovnik
	 * @param minPrah
	 * @param maxPrah
	 * @param krok
	 * @param metrika
	 * @throws IOException
	 */
	public void prohledejDokumentyKL(String indexDirectory, String testovaciDokumenty, String slovnik, double minPrah, double maxPrah,
			double krok, IMereni metrika) throws IOException {
		File trenovaci = new File(testovaciDokumenty);
		File[] files = trenovaci.listFiles();

		Directory directory = FSDirectory.open(Paths.get(indexDirectory));

		IndexReader reader = DirectoryReader.open(directory);
		IndexSearcher searcher = new IndexSearcher(reader);

		int pocetDokumentu = reader.numDocs();

		Map<String, Double>[] vektory = metrika.vratVahy(indexDirectory);
		Ner ner = Ner.load(slovnik);
		PojmenovaneEntity entity = new PojmenovaneEntity(ner);

		for (double i = minPrah; i <= maxPrah; i += krok) {
			int m = 0;
			double[] fMiry = new double[files.length];
			for (File file : files) {
				int pocetObdrzenych = 0;
				int pocetObdrzenychRelevantnich = 0;

				int cisloDokumentu = -1;

				String jmeno = file.getName();

				String[] rozdelene = jmeno.split("_");

				try {
					cisloDokumentu = Integer.parseInt(rozdelene[0]);
				} catch (Exception e) {

				}

				TermQuery query = new TermQuery(new Term("fileName", jmeno));

				TopDocs hits = searcher.search(query, 1);

				int docID = 0;

				for (ScoreDoc scoreDoc : hits.scoreDocs) {
					docID = scoreDoc.doc;
				}

				Document d = reader.document(docID);
				int pocetRelevantnich;
				if (d.get("custom:pocetrevizi") == null) {
					pocetRelevantnich = 0;
					pocetObdrzenychRelevantnich = 0;
				} else {
					pocetRelevantnich = (int) Double.parseDouble(d.get("custom:pocetrevizi"));
				}

//				System.out.println("\n Dokument " + d.get("fileName") + " ma mit pocet revizi "
//						+ d.get("custom:pocetrevizi") + " ma nasledujici revize\n");

				for (int k = 0; k < pocetDokumentu; k++) {
					if (docID == k) {
						continue;
					}
					double podobnost1 = metrika.vratMiru(vektory[k], vektory[docID]);
					double podobnost2 = metrika.vratMiru(vektory[docID], vektory[k]);
					if (podobnost1 < i || podobnost2 < i) {
						if (entity.zkontrolujRevize(reader, k, docID) == true
								&& entity.zkontrolujRevize(reader, docID, k) == true) {
							pocetObdrzenych++;
							Document a = reader.document(k);
							String[] split = a.get("fileName").split("_");
							try {
								int cislo = Integer.parseInt(split[0]);
								if (cisloDokumentu == cislo) {
									pocetObdrzenychRelevantnich++;
								}
							} catch (Exception e) {

							}
//							System.out.println(k + " " + a.get("fileName") + " pocet revizi "
//									+ a.get("custom:pocetrevizi") + " " + podobnost1 + " " + podobnost2);
						}
					}
				}

				if (pocetRelevantnich == 0 && pocetObdrzenych != 0) {
					fMiry[m] = 0;
				} else if (pocetObdrzenychRelevantnich == 0 && pocetRelevantnich != 0) {
					fMiry[m] = 0;
				} else {
					double mira = spoctiFMiru(pocetObdrzenychRelevantnich, pocetObdrzenych, pocetRelevantnich);
					fMiry[m] = mira;
				}
				m++;
			}
			System.out.println("Pro prah " + i + " je prumerna mira " + ziskejPrumer(fMiry));
		}

	}

	/**
	 *Pro vsechny testovaci dokumenty vyhleda revize a zmeri uspesnost, pricemz pouzije algoritmus More like this.
	 * @param indexDirectory
	 * @param testovaciDokumenty
	 * @param minPrah
	 * @param maxPrah
	 * @param krok
	 * @throws IOException
	 */
	public void prohledejDokumentyMLT(String indexDirectory, String testovaciDokumenty, double minPrah, double maxPrah,
			double krok) throws IOException {
		File trenovaci = new File(testovaciDokumenty);
		File[] files = trenovaci.listFiles();

		Directory directory = FSDirectory.open(Paths.get(indexDirectory));

		IndexReader reader = DirectoryReader.open(directory);
		IndexSearcher searcher = new IndexSearcher(reader);

		MoreLikeThis mlt = new MoreLikeThis(reader);
		mlt.setFieldNames(new String[] { "text" });

		for (double prah = minPrah; prah <= maxPrah; prah += krok) {
			int m = 0;
			double[] fMiry = new double[files.length];
			for (File file : files) {
				int pocetObdrzenych = 0;
				int pocetObdrzenychRelevantnich = 0;

				int cisloDokumentu = -1;

				String jmeno = file.getName();

				String[] rozdelene = jmeno.split("_");

				try {
					cisloDokumentu = Integer.parseInt(rozdelene[0]);
				} catch (Exception e) {
					
				}

				TermQuery query = new TermQuery(new Term("fileName", jmeno));

				TopDocs hits = searcher.search(query, 1);

				int docID = 0;

				for (ScoreDoc scoreDoc : hits.scoreDocs) {
					docID = scoreDoc.doc;
				}
				Document d = reader.document(docID);
				int pocetRelevantnich;
				if (d.get("custom:pocetrevizi") == null) {
					pocetRelevantnich = 0;
					pocetObdrzenychRelevantnich = 0;
				} else {
					pocetRelevantnich = (int) Double.parseDouble(d.get("custom:pocetrevizi"));
				}
				// System.out.println("\n Dokument " + d.get("fileName") + " ma
				// mit pocet revizi "
				// + d.get("custom:pocetrevizi") + " ma nasledujici revize\n");
				Query novy = mlt.like(docID);

				TopDocs vysledek = searcher.search(novy, 20);

				for (ScoreDoc scoreDoc : vysledek.scoreDocs) {
					if (scoreDoc.doc == docID) {
						continue;
					}
					if (scoreDoc.score > prah) {
						pocetObdrzenych++;
						Document a = reader.document(scoreDoc.doc);
						String[] split = a.get("fileName").split("_");
						try {
							int cislo = Integer.parseInt(split[0]);
							if (cisloDokumentu == cislo) {
								pocetObdrzenychRelevantnich++;
							}
						} catch (Exception e) {

						}
						// System.out.println(scoreDoc.doc + " " +
						// a.get("fileName") + " pocet revizi "
						// + a.get("custom:pocetrevizi") + " skore " +
						// scoreDoc.score);
					}
				}

				if (pocetRelevantnich == 0 && pocetObdrzenych != 0) {
					fMiry[m] = 0;
				} else if (pocetObdrzenych == 0 && pocetRelevantnich != 0) {
					fMiry[m] = 0;
				} else {
					double mira = spoctiFMiru(pocetObdrzenychRelevantnich, pocetObdrzenych, pocetRelevantnich);
					fMiry[m] = mira;
				}
				m++;
			}
			System.out.println("Pro prah " + prah + " je prumerna F-mira " + ziskejPrumer(fMiry));
		}
	}

	/*
	 * Z cisel v parametru spocte F-miru.
	 */
	public double spoctiFMiru(int obdrzeneRelevantni, int obdrzene, int relevantni) {

		if (obdrzeneRelevantni == 0 && obdrzene == 0 && relevantni == 0) {
			return 1;
		}

		double precision = (double) obdrzeneRelevantni / (double) obdrzene;
		double recall = (double) obdrzeneRelevantni / (double) relevantni;

		double delitel = precision + recall;

		if (delitel == 0) {
			return 0;
		}

		double mira = 2 * ((precision * recall) / delitel);

		return mira;
	}

	public double ziskejPrumer(double[] pole) {
		double vse = 0;
		for (int i = 0; i < pole.length; i++) {
			vse += pole[i];
		}
		return vse / pole.length;
	}

}
