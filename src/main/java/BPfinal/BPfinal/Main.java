package BPfinal.BPfinal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.lucene.analysis.cz.CzechAnalyzer;

import index.ShingleAnalyzer;
import index.VytvoreniIndexu;
import metriky.JaccarduvIndex;
import metriky.KLDivergence;
import metriky.KosinovaVzdalenost;
import spusteni.Spusteni;

/**
 * Hello world!
 *
 */
public class Main 
{
	/**
	 * nacte konfiguracni soubor a preda dale nactene parametry
	 * @param args
	 * @throws IOException
	 */
    public static void main( String[] args ) throws IOException
    {
    	Main app = new Main();
    	
    	if(args.length != 1){
    		System.out.println("Spatne zadane parametry programu!");
    		System.exit(1);
    	}
    	
    	BufferedReader bfReader = null;
    	
    	try {
    		File file = new File(args[0]);
			bfReader = new BufferedReader(new FileReader(file));
			List<String> radky = new ArrayList<>();
			String line;
			while((line = bfReader.readLine()) != null){
				radky.add(line);
			}
			String typ[] = radky.get(0).split(" ");
			if(typ.length != 2){
				System.out.println("Spatny format konfiguracniho souboru!");
				System.exit(1);
			}else{
				app.rozpoznejCoDal(typ, radky);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Konfiguracni soubor nenalezen!");
			e.printStackTrace();
			System.exit(1);
		}
    	
    }
    
    /**
     * Pozna, zda chce uzivatel vytvorit index nebo chce spustit nektery z experimentu
     * @param typ
     * @param radky
     * @throws IOException
     */
    public void rozpoznejCoDal(String[] typ, List<String> radky) throws IOException{
    	switch (typ[1]) {
		case "vytvorIndex":
			vytvorIndex(radky);
			break;
			
		case "experimentStavajici":
			ukazExperiment(radky);
			break;

		case "experimentNovy":
			ukazExperimentNovy(radky);
			break;
			
		default:
			break;
		}
    }
    
    /**
     * zjisti z konfiguracniho souboru parametry na vytvoreni indexu.
     * @param radky
     */
    public void vytvorIndex(List<String> radky){
    	VytvoreniIndexu index = new VytvoreniIndexu();
    	String typ[] = radky.get(1).split(" ");
    	if(typ.length != 2){
    		System.out.println("Spatny format konfirugacniho souboru!");
    		System.exit(1);
    	}
    	if(typ[1].equals("jednotliveTermy")){
    		String[] indexDirectory = radky.get(2).split(" ");
    		String[] documentDirectory = radky.get(3).split(" ");
    		try {
				index.createIndex(indexDirectory[1], documentDirectory[1], new CzechAnalyzer());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}else if(typ[1].equals("shingling")){
    		String[] velikostString = radky.get(2).split(" ");
    		int velikost = 0;
    		try{
    			velikost = Integer.parseInt(velikostString[1]);
    		}catch (NumberFormatException e) {
        		System.out.println("Spatny format konfirugacniho souboru!");
        		System.exit(1);
			}
    		String[] indexDirectory = radky.get(3).split(" ");
    		String[] documentDirectory = radky.get(4).split(" ");
    		try {
				index.createIndex(indexDirectory[1], documentDirectory[1], new ShingleAnalyzer(velikost, velikost));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}else{
    		System.out.println("Spatny format konfiguracniho souboru!");
    		System.exit(1);
    	}
    }
    
    /**
     * zobrazi experiment s parametry definovanymi uzivatelem v konfiguracnim souboru.
     * @param radky
     */
    public void ukazExperiment(List<String> radky){
    	String[] typ = radky.get(1).split(" ");
    	Spusteni spusteni = new Spusteni();
		double minPrah = 0;
		double maxPrah = 0;
		double krok = 0;
    	switch (typ[1]) {
		case "jednotliveTermy":
			String[] parametry = nactiParametry(3, 7, radky);
			try {
				minPrah = Double.parseDouble(parametry[2]);
				maxPrah = Double.parseDouble(parametry[3]);
				krok = Double.parseDouble(parametry[4]);
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				spusteni.prohledejDokumenty(parametry[0], parametry[1], minPrah, maxPrah, krok, new KosinovaVzdalenost());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			
		case "shingling":
			String[] parametryShingle = nactiParametry(3, 8, radky);
			try {
				minPrah = Double.parseDouble(parametryShingle[3]);
				maxPrah = Double.parseDouble(parametryShingle[4]);
				krok = Double.parseDouble(parametryShingle[5]);
			} catch (Exception e) {
				// TODO: handle exception
			}
			if(parametryShingle[0].equals("kosinovaVzdalenost")){
				try {
					spusteni.prohledejDokumenty(parametryShingle[1], parametryShingle[2], minPrah, maxPrah, krok, new KosinovaVzdalenost());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(parametryShingle[0].equals("jaccarduvIndex")){
				try {
					spusteni.prohledejDokumenty(parametryShingle[1], parametryShingle[2], minPrah, maxPrah, krok, new JaccarduvIndex());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}else{
	    		System.out.println("Spatny format konfiguracniho souboru!");
	    		System.exit(1);			
			}
			break;

		case "mlt":
			String[] parametryMLT = nactiParametry(3, 7, radky);
			try {
				minPrah = Double.parseDouble(parametryMLT[2]);
				maxPrah = Double.parseDouble(parametryMLT[3]);
				krok = Double.parseDouble(parametryMLT[4]);
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				spusteni.prohledejDokumentyMLT(parametryMLT[0], parametryMLT[1], minPrah, maxPrah, krok);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		default:
			break;
		}
    }
    
    /**
     * spusti testovani nove vytvoreneho algoritmu.
     * @param radky
     */
    public void ukazExperimentNovy(List<String> radky){
    	Spusteni spusteni = new Spusteni();
    	String[] parametry = nactiParametry(2, 7, radky);
		double minPrah = 0;
		double maxPrah = 0;
		double krok = 0;
		try {
			minPrah = Double.parseDouble(parametry[3]);
			maxPrah = Double.parseDouble(parametry[4]);
			krok = Double.parseDouble(parametry[5]);
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			spusteni.prohledejDokumentyKL(parametry[0], parametry[1], parametry[2], minPrah, maxPrah, krok, new KLDivergence());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public String[] nactiParametry(int zacatek, int konec, List<String> radky){
    	String[] parametry = new String[zacatek + konec + 1];
    	int index = 0;
    	for(int i = (zacatek-1); i < konec; i++){
    		parametry[index] = radky.get(i).split(" ")[1];
    		index++;
    	}
    	return parametry;
    }
}
