package metriky;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

public class KLDivergence implements IMereni{

	/**
	 * spocte hodnotu KL divergence mezi dvema vektory ohodnocene pomoci frekvenci termu.
	 */
	@Override
	public double vratMiru(Map<String, Double> v1, Map<String, Double> v2) {
		double sum1 = spoctiSumu(v1);
		double sum2 = spoctiSumu(v2);
		double vysledek = 0.0;
		for (String string : v1.keySet()) {
			double frekvence2 = 0.0;
			double frekvence1 = (double) v1.get(string) / sum1;
			if (v2.containsKey(string)) {
				frekvence2 = (double) v2.get(string) / sum2;
			}else{
				frekvence2 = 0.1 * Math.pow(10, -10);
			}
			double prirustek = frekvence1 * (Math.log(frekvence1 / frekvence2));
			vysledek += prirustek;
		}
		return vysledek;
	}
	
	/**
	 * pro normalizaci vektoru tak, aby se jejich suma rovnala 1 je treba vydelit kazdy prvek sumou prvku vektoru. Tato metoda ziska sumu z vektoru.
	 * @param vektor
	 * @return
	 */
	public double spoctiSumu(Map<String, Double> vektor){
		double suma = 0;
		for (String string : vektor.keySet()) {
			suma += vektor.get(string);
		}
		return suma;
	}

	/**
	 * ziska ohodnocene vektory ze vsech dokumentu v indexu.
	 */
	@Override
	public Map<String, Double>[] vratVahy(String indexPath) throws IOException {
		Directory directory = FSDirectory.open(Paths.get(indexPath));
		
		IndexReader reader = DirectoryReader.open(directory);
		
		int pocetDokumentu = reader.numDocs();
		
		Map<String, Double>[] vektoryDokumentu = new HashMap[pocetDokumentu];

		for(int i = 0; i < pocetDokumentu; i++){
			vektoryDokumentu[i] = getDokumentVektor(reader, i);
		}
		return vektoryDokumentu;
	}
	
	public Map<String, Double> getDokumentVektor(IndexReader reader, int docId) throws IOException {
		Terms vector = reader.getTermVector(docId, "text");
		Map<String, Double> termFrekvence = new HashMap<String, Double>();
		TermsEnum termsEnum = null;
		termsEnum = vector.iterator();
		BytesRef text = null;
		while ((text = termsEnum.next()) != null) {
			String term = text.utf8ToString();
			termFrekvence.put(term, (double) termsEnum.totalTermFreq());
		}
		return termFrekvence;
		
/*		Terms vector = reader.getTermVector(docId, "text");
		Map<String, Integer> dokumentFrekvence = new HashMap<String, Integer>();
		Map<String, Integer> termFrekvence = new HashMap<String, Integer>();
		Map<String, Double> tfIdfFrekvence = new HashMap<String, Double>();
		TermsEnum termsEnum = null;
		termsEnum = vector.iterator();
		BytesRef text = null;
		int pocetSlovVdokumentu = 0;
		while ((text = termsEnum.next()) != null) {
			String term = text.utf8ToString();
			pocetSlovVdokumentu += termsEnum.totalTermFreq();
			dokumentFrekvence.put(term, reader.docFreq(new Term("text", term)));
			termFrekvence.put(term, (int) termsEnum.totalTermFreq());
		}
		for (String term : dokumentFrekvence.keySet()) {
			double tf = (double) termFrekvence.get(term) / (double) pocetSlovVdokumentu;
			double idf = Math.log((double)reader.maxDoc()/(double)dokumentFrekvence.get(term));
			double vaha = tf * idf;
			tfIdfFrekvence.put(term, vaha);
		}
		return tfIdfFrekvence;*/
	}

}
