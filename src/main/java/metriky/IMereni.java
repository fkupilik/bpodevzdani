package metriky;

import java.io.IOException;
import java.util.Map;

public interface IMereni {
	
	public double vratMiru(Map<String, Double> v1, Map<String, Double> v2);
	
	public Map<String, Double>[] vratVahy(String indexPath) throws IOException;

}
