package metriky;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

public class KosinovaVzdalenost implements IMereni{
	
    private Set<String> vsechnaSlova = null;

    /**
     * Spocte kosinovou vzdalenost mezi dvema vektory ohodnocene TF-IDF
     */
	@Override
	public double vratMiru(Map<String, Double> v1, Map<String, Double> v2) {
    	double skalarSoucin = 0;
    	double norm1 = 0;
    	double norm2 = 0;
    	for (String string : v1.keySet()) {
			skalarSoucin += v1.get(string) * v2.get(string);
			norm1 += Math.pow(v1.get(string), 2);
			norm2 += Math.pow(v2.get(string), 2);
		}
    	return skalarSoucin/(Math.sqrt(norm1) * Math.sqrt(norm2));
	}

	/**
	 * ziska ze vsech dokumentu v indexu jejich vektory ohodnocene TF-IDF
	 */
	@Override
	public Map<String, Double>[] vratVahy(String indexPath) throws IOException{
		vsechnaSlova = new HashSet<>();
		Directory directory = FSDirectory.open(Paths.get(indexPath));
		
		IndexReader reader = DirectoryReader.open(directory);
		
		int pocetDokumentu = reader.numDocs();
		Map<String, Double>[] mapyDokumentu = new HashMap[pocetDokumentu];
		Map<String, Double>[] mapyDokumentuVprostoru = new HashMap[pocetDokumentu];
		
		for(int i = 0; i < pocetDokumentu; i++){
			mapyDokumentu[i] = getDokumentVektor(reader, i);
		}
		for(int i = 0; i < pocetDokumentu; i++){
			mapyDokumentuVprostoru[i] = getSpaceVector(mapyDokumentu[i]);
		}
		return mapyDokumentuVprostoru;
	}
	
	/**
	 * ohodnoti dokument z indexu definovany ID pomoci TF-IDF
	 * @param reader
	 * @param docId
	 * @return
	 * @throws IOException
	 */
	public Map<String, Double> getDokumentVektor(IndexReader reader, int docId) throws IOException {
		Terms vector = reader.getTermVector(docId, "text");
		Map<String, Integer> dokumentFrekvence = new HashMap<String, Integer>();
		Map<String, Integer> termFrekvence = new HashMap<String, Integer>();
		Map<String, Double> tfIdfFrekvence = new HashMap<String, Double>();
		TermsEnum termsEnum = null;
		termsEnum = vector.iterator();
		BytesRef text = null;
		int pocetSlovVdokumentu = 0;
		while ((text = termsEnum.next()) != null) {
			String term = text.utf8ToString();
			pocetSlovVdokumentu += termsEnum.totalTermFreq();
			dokumentFrekvence.put(term, reader.docFreq(new Term("text", term)));
			termFrekvence.put(term, (int) termsEnum.totalTermFreq());
			vsechnaSlova.add(term);
		}
		for (String term : dokumentFrekvence.keySet()) {
			double tf = (double) termFrekvence.get(term) / (double) pocetSlovVdokumentu;
			double idf = Math.log((double)reader.maxDoc()/(double)dokumentFrekvence.get(term));
			double vaha = tf * idf;
			tfIdfFrekvence.put(term, vaha);
		}
		return tfIdfFrekvence;
	}
	
    public Map<String, Double> getSpaceVector(Map<String, Double> map){
    	Map<String, Double> vektor = new HashMap<String, Double>();
    	for (String slovo : vsechnaSlova) {
    		if(map.containsKey(slovo)){
    			vektor.put(slovo, map.get(slovo));
    		}else{
    			vektor.put(slovo, (double) 0);
    		}    		
		}
    	return vektor;
    }

}
