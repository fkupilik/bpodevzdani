package metriky;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

public class JaccarduvIndex implements IMereni{

	/**
	 * Spocte Jaccarduv index mezi dvema mnozinami ohodnocenymi booleanovskymi hodnotami.
	 */
	@Override
	public double vratMiru(Map<String, Double> v1, Map<String, Double> v2) {
    	Set<String> union = new HashSet<String>();
    	union.addAll(v1.keySet());
    	union.addAll(v2.keySet());
    	
    	int prunik = v1.size() + v2.size() - union.size();
    	return (double)prunik/union.size();
	}

	/**
	 * Ziska z indexu vektory dokumentu ohodnocene booleovskymi hodnotami.
	 * @throws IOException 
	 */
	@Override
	public Map<String, Double>[] vratVahy(String indexPath) throws IOException {
		Directory directory = FSDirectory.open(Paths.get(indexPath));
		
		IndexReader reader = DirectoryReader.open(directory);
		
		int pocetDokumentu = reader.numDocs();
		
		Map<String, Double>[] vektoryDokumentu = new HashMap[pocetDokumentu];

		for(int i = 0; i < pocetDokumentu; i++){
			vektoryDokumentu[i] = getDokumentVektor(reader, i);
		}
		return vektoryDokumentu;
	}
	
	public Map<String, Double> getDokumentVektor(IndexReader reader, int docId) throws IOException{
        Terms vector = reader.getTermVector(docId, "text");
        Map<String, Double> termFrequencies = new HashMap<String, Double>();
        TermsEnum termsEnum = vector.iterator();
        BytesRef text = null;
        
        while ((text = termsEnum.next()) != null) {
            String term = text.utf8ToString();
            termFrequencies.put(term, (double)1);  
        }
        
        return termFrequencies;
	}

}
